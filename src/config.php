<?php

namespace Credits;

class Config
{

	public $dbConfig = array(
		""
	);
	
	public static $params = array(
		"installments" => array("From" => 3, "To" => 24, "Step" => 1),
		"amount" => array("From" => 500, "To" => 5000, "Step" => 100),
		"air" => 10,
		"maturityDate" => array(10 => "10-то число", 20 => "20-то число", "EOM" => "В края на месеца"),
		"taxes" => array("tax1" => 25, "tax2" => 17), 
		"dateFormat" => "Y-m-d"
	); 

}