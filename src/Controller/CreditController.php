<?php

namespace Credits\Controller;

class CreditController
{
	private $error = "";

	function __construct() 
	{

	}	


	//Load index page	
	//@return string - index page	
	public function index()
	{
		$veiw = new \Credits\System\View();

		$data = array(
			"instalments" => \Credits\Config::$params["installments"],
			"amount" => \Credits\Config::$params["amount"],
			"maturityDate" => \Credits\Config::$params["maturityDate"] 
		);

		$veiw->render("header.php", $data);
		$veiw->render("credit-form.php", $data);
		$veiw->render("footer.php", $data);

		echo $veiw->out();
	}
	
	//Load prepare and schedile table 
	//@return json	
	public function calcSchedule()
	{
		$veiw = new \Credits\System\View();
	
		$post = $_POST; 

		if (! $this->validatForm($post)) {
			$this->error();	
		} else {
			$calculator = new \Credits\System\Calculator(\Credits\Config::$params);

			$data = array(
				"taxes" => \Credits\Config::$params["taxes"]
				, "data" => $calculator->schedule($post)
			);

			$veiw->render("schedule-table.php", $data);

			header("Content-Type: application/json; charset=utf-8");
			echo json_encode(array("html" => $veiw->out()));
		}
	} 


	//Load register form
	//@return string  - html	
	public function registerForm()
	{
		$veiw = new \Credits\System\View();

		$veiw->render("register-form.php");
		echo $veiw->out();
	}


	//Validating calcolator data
	//@param array $post - post data 
	//@return boolean 
	private function validatForm($post)
	{
	
		$error = "";
		$instalments = \Credits\Config::$params["installments"];
		$amount = \Credits\Config::$params["amount"];
		$maturityDate = \Credits\Config::$params["maturityDate"]; 
	
		if (! trim($post["amaunt"]) || ($amount["From"] > $post["amaunt"]) || ($post["amaunt"] > $amount["To"]) ) {
			$this->error = "Невалидна стойност на сумата";
			return false;
		} 

		if (! trim($post["installments"]) || ($instalments["From"] > $post["installments"]) || ($post["installments"] > $instalments["To"]) ) {
			$this->error = "Невалиден брой вноски";
			return false;
		} 


		if (! isset($maturityDate[$post["maturityDate"]])) {
			$this->error = "Невалиден падеж";
			return false;
		} 
		
		if (trim($post["utilisationDate"]) == "") {
			$this->error = "Невалидна дата на падеж";
			return false;
		} 

		try {
			$m = new \Moment\Moment($post["utilisationDate"]);
		} catch(\Moment\MomentException $e) {
			$this->error = "Невалидна дата на падеж";
			return false;
//			echo 'Message: ' .$e->getMessage();
		}

		return true;
	}


	//Validating client info
	//@return json
	public function validate()
	{
		$utility = new \Credits\System\Utility();
	
		$post = $_POST; 
	
		if (! $this->validatForm($post)) {
			$this->error();
		}

		if (! isset($post["Name"]) || ! $utility->validateName($post["Name"])) {
			$this->error = "Невалидно име";
			$this->error();
			return;
		}

		if (! isset($post["Phone"]) || ! $utility->validatePhone($post["Phone"])) {
			$this->error = "Невалиден телефонен номер";
			$this->error();
			return;
		}

		if (empty($post["confirm"])) {
			$this->error = "За да продължите трябва да сте съгласни с условията ни";
			$this->error();
			return;
		}

		header("Content-Type: application/json; charset=utf-8");
		echo json_encode(array("result" => "OK"));
	}

	//Load confirm page
	//@return string - html
	public function confirm()
	{
		$veiw = new \Credits\System\View();

		$post = $_POST; 

		$veiw->render("header.php");
		$veiw->render("confirm.php", $post);
		$veiw->render("footer.php");
		echo $veiw->out();
	}

	//Error reporting
	private function error()
	{
		header("Content-Type: application/json; charset=utf-8");
		echo json_encode(array("error" => $this->error));
	}
}