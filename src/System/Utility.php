<?php

namespace Credits\System;

class Utility
{

	//Validate Name
	//@param string $name
	//@return boolean 
	public function validateName($name) {
		return (strlen(trim($name)) >= 10);
	}

	//Validate Phone
	//@param string $phone 
	//@return boolean 
	public function validatePhone($phone) {
		return (preg_replace("/[^0-9 ]/", "", $phone) == $phone) && (strlen($phone) >= 9);
	}
} 
