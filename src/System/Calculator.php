<?php

namespace Credits\System;

//Calculate annuity installments
class Calculator 
{
	//Calculator params
	//@type array
	private $params = array();

	
	public function __construct($params)
	{
		$this->params = $params;
	}

	//Create annuity installments
	//@param array $post - form post 
	//@return array
	public function schedule($post)
	{
		$data = array();

		$m = new \Moment\Moment($post["utilisationDate"]);
		$PrevDate = $m->cloning()->subtractDays(1);
	
		$air = $this->params["air"];
		$taxes = $this->params["taxes"]; 
		$amaunt = $principal = $post["amaunt"];
	
		$taxSum = 0;
		$corr = array(
			"installmentAmount" => 0,
			"principal" => 0,
			"interest" => 0,
			"taxes" => array() 
		);
		foreach($taxes as $k => $v) {
			$taxSum += $v/$post["installments"];
			$corr["taxes"][$k] = 0;
		}
		
		$mk = $air/100/12;
		$k = (1 - (1/((1 + $mk)**$post["installments"]) ) );
		$installmentAmount = $amaunt / ($k / $mk);
	
		$balance = $amaunt; 
		for ($i = 1; $i <= $post["installments"]; $i++) {
		
			if ("EOM" == $post["maturityDate"]) {
				$date = $m->cloning();
				$date = $date->endOf("month"); 
			} else {// $post["maturityDate"]
				$day = (int)$m->getDay();
				$month = $m->getMonth();
				$year = $m->getYear();
	
				if ($post["maturityDate"] > $day ) {
					$date = $m->cloning();
					$date->addDays($post["maturityDate"] - $day);
				} else {
					$date = new \Moment\Moment($year."-".str_pad((int)$month+1, 2, "0", STR_PAD_LEFT)."-".(int)$post["maturityDate"]); 
				}	
			}
			$momentFromVo = $PrevDate->from($date->format($this->params["dateFormat"])); 	
	
			$interest = ($balance*$air/100)/12; 
			$balance -= $installmentAmount - $interest;

			$corr["installmentAmount"] += ($installmentAmount + $taxSum) - round($installmentAmount + $taxSum, 2); 	
			$corr["interest"] += $interest - round($interest, 2); 	

			if ($i == $post["installments"]) {
				$installmentAmount += $corr["installmentAmount"];
				$interest += $corr["interest"];
			}

			$row = array(
				'number' => $i,
				'date' => $date->format($this->params["dateFormat"]),
				'period' => $momentFromVo->getDays(),
				'installmentAmount' => round($installmentAmount + $taxSum, 2),
				'principal' => round($installmentAmount - $interest, 2),
				'interest' => round($interest, 2),
			);
	
			foreach($taxes as $k => $v) {
				$tax = $v/$post["installments"]; 
				$corr["taxes"][$k] += $tax - round($tax, 2);
				if ($i == $post["installments"])
					$tax += $corr["taxes"][$k];
				$row[$k] = round($tax, 2);
			}
	
			$data[] = $row;
	
			$PrevDate = $date->cloning()->subtractDays(1);
			$m->addMonths(1);
		}
	
		return $data;
	}

}

