<?php

namespace Credits\System;

class View
{
	//Output buffer
	//@type string - html
	protected $out = ""; 

	function __construct() 
	{
	}	

	//Render template
	public function render($template, $data = array())
	{
		extract($data);

  		ob_start();
		include (APP_ROOT_FOLDER."/src/View/".$template);
  		$this->out .= ob_get_contents();
  		ob_end_clean();
	}

	//Outputting buffer
	//@retrun string - html 
	public function out()
	{
		return $this->out;
	}
}