<?php

namespace Credits\System;

class Routes 
{
	//Define routes
	private static $routes = array(
		"calc" => "calcSchedule",
		"register" => "registerForm", 
		"validate" => "validate",
		"confirm" => "confirm"
	);

	//Default controller method 
	private static $defaultRoute = "index"; 

	//Call controller methods
	//@param CreditController $controller
	public static function get($controller)
	{
	
		$route = "";
		if (count($_GET))
			$route = array_keys($_GET)[0];

		if ($route && isset(self::$routes[$route])) {
			$function = self::$routes[$route];
		} else {
			$function = self::$defaultRoute;
		}

		$controller->$function();
	}
}