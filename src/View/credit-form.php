
<form action="" method="post" id="mainForm">

<div class="row form-group mt-5">
	<div class="col-sm-6 col-12">
		<h3 class="text-center">Сума <span id="show_amount"></span></h3>
		<div class="slidecontainer">
			<input type="range" min="<?php echo $amount["From"]; ?>" max="<?php echo $amount["To"]; ?>" value="<?php echo round($amount["To"]/2); ?>" step="<?php echo round($amount["Step"]); ?>" class="slider" id="amount">
		</div>
		<input type="hidden" value="<?php echo round($amount["To"]/2); ?>" name="amaunt">
	</div>
	<div class="col-sm-6 col-12">
		<h3 class="text-center">Вноски <span id="show_installments"></span></h3>
		<div class="slidecontainer">
			<input type="range" min="<?php echo $instalments["From"]; ?>" max="<?php echo $instalments["To"]; ?>" value="<?php echo round($instalments["To"]/2); ?>" step="<?php echo round($instalments["Step"]); ?>" class="slider" id="installments">
		</div>
		<input type="hidden" value="<?php echo round($instalments["To"]/2); ?>" name="installments">
	</div>
</div>

<div class="row form-group">
	<div class="col-sm-6 col-12">
		<div class="row">
			<label class="col-3 form-control-label">Дата</label>
			<input type="text" value="" name="utilisationDate" class="col-9 form-control">
		</div>
	</div>
	<div class="col-sm-6 col-12">
		<div class="row">
			<label class="col-3 form-control-label">Падеж</label>
			<select name="maturityDate" class="col-9 custom-select" required>
			<?php foreach ($maturityDate as $k => $d) { ?>
				<option value="<?php echo $k; ?>"><?php echo $d; ?></option>
			<?php } ?>
			</select>
		</div>
	</div>
</div>

<div class="row mt-5">
	<div class="col-4 offset-sm-4 text-center">
		<input type="button" value="Изчисли" id="calcSchedule" class="btn btn-lg btn-block btn-dark">
	</div>
</div>

<div id="plan_canvas"></div>


<div id="register_canvas"></div>

</form>