	<div class="form-group row">
		<label for="inputName3" class="col-sm-2 col-form-label">Име</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" id="inputName3" name="Name" placeholder="Име">
		</div>
	</div>
	<div class="form-group row">
		<label for="inputPhone3" class="col-sm-2 col-form-label">Телефон</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" id="inputPhone3" name="Phone" placeholder="Телефон">
		</div>
	</div>
	<div class="form-group row">
		<div class="col-sm-2">Условия</div>
		<div class="col-sm-10">
			<div class="form-check">
				<input class="form-check-input" type="checkbox" name="confirm" id="gridCheck1">
				<label class="form-check-label" for="gridCheck1">
					Запознат съм с условията
				</label>
			</div>
		</div>
	</div>

<div class="row my-5">
	<div class="col-4 offset-sm-4 text-center">
		<input type="submit" value="Изпрати" id="confirm" class="btn btn-lg btn-block btn-success">
	</div>
</div>
