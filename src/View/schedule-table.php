<table class="table table-bordered mt-5">
	<thead>
		<tr>
			<th>#</th>
			<th>Падеж</th>
			<th>Период</th>
			<th>Вноска</th>
			<th>Главница</th>
			<th>Лихва</th>
			<?php foreach(array_keys($taxes) as $t) { ?>
			<th><?php echo $t; ?></th>
			<?php } ?>
		</tr>
	</thead>
	<tbody>
	<?php foreach($data as $row) { ?>
		<tr>
			<th><?php echo $row["number"]; ?></th>
			<td><?php echo $row["date"]; ?></td>
			<td><?php echo $row["period"]; ?></td>
			<td><?php echo $row["installmentAmount"]; ?></td>
			<td><?php echo $row["principal"]; ?></td>
			<td><?php echo $row["interest"]; ?></td>
			<?php foreach(array_keys($taxes) as $t) { ?>
			<td><?php echo $row[$t]; ?></td>
			<?php } ?>
		</tr>
	<?php } ?>
	</tbody>
</table>

<div class="row my-5">
	<div class="col-4 offset-sm-4 text-center">
		<input type="button" value="Кандидатствай" id="regform" class="btn btn-lg btn-block btn-warning">
	</div>
</div>
