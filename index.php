<?php

include_once (realpath("vendor/autoload.php"));  

define("APP_ROOT_FOLDER", __DIR__);

$c = new \Credits\Controller\CreditController();

\Credits\System\Routes::get($c);
