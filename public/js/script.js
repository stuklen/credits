
$(function() {

	$('#show_amount').html($('#amount').val());
	$('input[name="amaunt"]').val($('#amount').val());
	$('#show_installments').html($('#installments').val());
	$('input[name="installments"]').val($('#installments').val());
	
	$('#amount').on('change mousemove', function () {
		$('#show_amount').html($(this).val());
		$('input[name="amaunt"]').val($(this).val());
	});
	
	$('#installments').on('change mousemove', function () {
		$('#show_installments').html($(this).val());
		$('input[name="installments"]').val($(this).val());
	});


	$(document).on('click', '#calcSchedule', function (e) {
		e.preventDefault(); // avoid to execute the actual submit of the form.

		$.ajax({
			type: "POST",
			url: 'calc',
			data: $('#mainForm').serialize(), // serializes the form's elements.
   			dataType: 'json',
			success: function(data) {
				if (data.error) {
					$('#error-modal .modal-body').html(data.error);
					$('#error-modal').modal({show: true});
				} else {
					$('#plan_canvas').html(data.html);			
					$('#register_canvas').html('');			
				}
			}
		});
	});

	$(document).on('click', '#regform', function (e) {
		e.preventDefault(); // avoid to execute the actual submit of the form.

		$.ajax({
			type: "POST",
			url: 'register',
			data: $('#mainForm').serialize(), // serializes the form's elements.
			success: function(data) {
				$('#mainForm').append(data);			
				setTimeout( function () {
					$([document.documentElement, document.body]).animate({
						scrollTop: $("#register_canvas").offset().top
					}, 2000);
				}, 100);					
			}
		});
	});

	$(document).on('click', '#confirm', function (e) {
		e.preventDefault(); // avoid to execute the actual submit of the form.

		var dataString = $('#mainForm').serialize();

		$.ajax({
			type: "POST",
			url: 'validate',
			data: dataString, // serializes the form's elements.
   			dataType: 'json',
   			success: function(data) {
				if (data.error) {
					$('#error-modal .modal-body').html(data.error);
					$('#error-modal').modal({show: true});
				} else {
					$('#mainForm').attr('action', 'confirm');
console.log('submit');
					$('#mainForm').submit();			
				}
			}
		});
	});

});